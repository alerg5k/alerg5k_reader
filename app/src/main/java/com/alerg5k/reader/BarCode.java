package com.alerg5k.reader;

import android.util.Log;

import java.util.Date;

/**
 * Created by cipri on 05.08.2016.
 */
public class BarCode {

    private static final String LOG_TAG = "Alerg5K_Reader";

    public BarCode(CodeType type, int code) {
        this.type = type;
        this.code = code;
        this.date = new Date();
        Log.d(LOG_TAG, "new barcode:" + String.valueOf(code));
    }

    public enum CodeType {
        CODE_128,
        CODE_64
    }

    public CodeType getType() {
        return type;
    }

    public void setType(CodeType type) {
        this.type = type;
    }

    private CodeType type;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    private int code;

    public Date getDate() { return date; }

    private Date date;

    @Override
    public String toString() {
        return String.valueOf(this.code);
    }

    public String toExportString() {
        if (date == null) {
            date = new Date();
        }
        return "\"" + String.valueOf(this.code) + "\",\"CODE_128\",\"" + date.toString() + "\"\n";
    }
}
