package com.alerg5k.reader;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.alerg5k.reader.util.TinyDB;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class BarcodeListDialogFragment extends DialogFragment {

    //private String mEventName = "";
    private View mView;
    private Context mContext;
    private TinyDB mTinydb;
    private String mEvent = "";
    private ArrayList<BarCode> mBarcodes;
    private ArrayAdapter<BarCode> arrayAdapter;
    private boolean mChanged;

    public BarcodeListDialogFragment() {
        // Required empty public constructor
    }

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passesc the DialogFragment in case the host needs to query it. */
    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.getClass().toString()
                    + " must implement NoticeDialogListener");
        }
        mTinydb = new TinyDB(context);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        mEvent = mTinydb.getString("eventName");
        mBarcodes = mTinydb.getListBarCodes("barcodes");
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        mView = inflater.inflate(R.layout.dialog_barcodelist, null);
        // Add action buttons
        TextView tv = (TextView) mView.findViewById(R.id.textView4);
        tv.setText(mEvent);
        ListView lv = (ListView)  mView.findViewById(R.id.listView);
        arrayAdapter = new ArrayAdapter<BarCode>(
                mContext,
                android.R.layout.simple_list_item_1,
                mBarcodes );
        lv.setAdapter(arrayAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d("LV", "click on item: " + String.valueOf(mBarcodes.get((int) l).getCode()));
                final int mClickedId = (int) l;
                AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
                alert.setTitle("Are you sure you want to delete this code: " + mBarcodes.get((int) l) + "?");
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (!mBarcodes.isEmpty()) {
                            mBarcodes.remove(mClickedId);
                            arrayAdapter.notifyDataSetChanged();
                            mChanged = true;
                        }
                    }
                });
                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //Put actions for CANCEL button here, or leave in blank
                    }
                });
                alert.show();
            }
        });
        mChanged = false;

        builder.setView(mView);
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Send the positive button event back to the host activity
                        //EditText ed = (EditText) mView.findViewById(R.id.editText);
                        //mEventName = ed.getText().toString();
                        if (mChanged) {
                            mTinydb.putListBarcodes("barcodes", mBarcodes);
                            mListener.onDialogPositiveClick(BarcodeListDialogFragment.this);
                        }
                    }
                });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //nothing to do
            }
        });
        return builder.create();
    }

    /*public String getEventName() {
        return this.mEventName;
    }*/
}
