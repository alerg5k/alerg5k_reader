package com.alerg5k.reader;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.alerg5k.reader.util.TinyDB;
import com.microblink.geometry.Rectangle;
import com.microblink.hardware.camera.CameraType;
import com.microblink.recognition.InvalidLicenceKeyException;
import com.microblink.recognizers.BaseRecognitionResult;
import com.microblink.recognizers.RecognitionResults;
import com.microblink.recognizers.blinkbarcode.BarcodeType;
import com.microblink.recognizers.blinkbarcode.bardecoder.BarDecoderRecognizerSettings;
import com.microblink.recognizers.blinkbarcode.bardecoder.BarDecoderScanResult;
import com.microblink.recognizers.settings.RecognitionSettings;
import com.microblink.recognizers.settings.RecognizerSettings;
import com.microblink.recognizers.settings.RecognizerSettingsUtils;
import com.microblink.results.barcode.BarcodeDetailedData;
import com.microblink.util.RecognizerCompatibility;
import com.microblink.view.CameraAspectMode;
import com.microblink.view.CameraEventsListener;
import com.microblink.view.recognition.RecognizerView;
import com.microblink.view.recognition.ScanResultListener;

import java.util.ArrayList;

public class ActivityScan extends Activity implements ScanResultListener, CameraEventsListener, BarcodeListDialogFragment.NoticeDialogListener {

    private static final int PERMISSION_CAMERA_REQUEST_CODE = 69;
    private static final String LOG_TAG = "Alerg5K_Reader";
    private RecognizerView mRecognizerView;
    private String lastCode = "";
    private ImageButton mGetBarcodesBtn, mScanAgainBtn, mDeleteLastBtn, mClearBtn, mUnknownBtn, mAddCodeBtn, mValidateBtn;
    private TextView barCodeText;
    private TinyDB mTinydb;
    private String mEvent = "";
    private ArrayList<BarCode> mBarcodes;
    private boolean mHasEvent;

    private RecognizerSettings[] setupSettingsArray() {
        BarDecoderRecognizerSettings sett = new BarDecoderRecognizerSettings();
        // activate scanning of Code39 barcodes
        sett.setScanCode39(true);
        // activate scanning of Code128 barcodes
        sett.setScanCode128(true);
        // disable scanning of white barcodes on black background
        sett.setInverseScanning(false);
        // disable slower algorithm for low resolution barcodes
        sett.setTryHarder(false);

        // now add sett to recognizer settings array that is used to configure
        // recognition
        return new RecognizerSettings[] { sett };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // create RecognizerView
        mRecognizerView = new RecognizerView(this);

        RecognitionSettings settings = new RecognitionSettings();
        // setup array of recognition settings (described in chapter "Recognition
        // settings and results")
        RecognizerSettings[] settArray = setupSettingsArray();
        if (!RecognizerCompatibility.cameraHasAutofocus(CameraType.CAMERA_BACKFACE, this)) {
            settArray = RecognizerSettingsUtils.filterOutRecognizersThatRequireAutofocus(settArray);
        }
        settings.setRecognizerSettingsArray(settArray);
        mRecognizerView.setRecognitionSettings(settings);

        try {
            // set license key
            mRecognizerView.setLicenseKey("JPCFCWX2-J4XCYT3L-F6Q4SAZH-TJZ3QHVA-4UJGNGTT-XAPKBZIS-M2NHHOA6-UDSUGAOT");
        } catch (InvalidLicenceKeyException exc) {
            finish();
            return;
        }

        // scan result listener will be notified when scan result gets available
        mRecognizerView.setScanResultListener(this);
        // camera events listener will be notified about camera lifecycle and errors
        mRecognizerView.setCameraEventsListener(this);

        // set camera aspect mode
        // ASPECT_FIT will fit the camera preview inside the view
        // ASPECT_FILL will zoom and crop the camera preview, but will use the
        // entire view surface
        mRecognizerView.setAspectMode(CameraAspectMode.ASPECT_FILL);


        // add ROI layout
        View roiView = getLayoutInflater().inflate(R.layout.roi_overlay, null);
        mRecognizerView.addChildView(roiView, false);

        //RelativeLayout relLayout = setLayout();
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.activity_scan, null);
        mRecognizerView.addChildView(v, false);

        //set scanning region
        mRecognizerView.setScanningRegion(new Rectangle(0.2f, 0.1f, 0.6f, 0.6f), false);
        mRecognizerView.setInitialScanningPaused(true);
        mRecognizerView.create();

        setContentView(mRecognizerView);
        mTinydb = new TinyDB(this);
        mScanAgainBtn = (ImageButton) findViewById(R.id.imgBtnScan);
        mScanAgainBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRecognizerView.resumeScanning(true);
                barCodeText.setText("Scanning...");
                view.setEnabled(false);
            }
        });
        mScanAgainBtn.setEnabled(true);

        mGetBarcodesBtn = (ImageButton) findViewById(R.id.imgBtnList);
        mGetBarcodesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTinydb.putListBarcodes("barcodes", mBarcodes);
                new BarcodeListDialogFragment().show(getFragmentManager(), "dialoglist");
            }
        });

        mUnknownBtn = (ImageButton) findViewById(R.id.imgBtnUnknown);
        mUnknownBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mTinydb.putListBarcodes("barcodes", mBarcodes);
                mBarcodes.add(new BarCode(BarCode.CodeType.CODE_128, 678));
                ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                toneG.startTone(ToneGenerator.TONE_DTMF_9, 200);
                lastCode = "00678";
                barCodeText.setText("00678");
                //new BarcodeListDialogFragment().show(getFragmentManager(), "dialoglist");
            }
        });

        mAddCodeBtn = (ImageButton) findViewById(R.id.imgBtnAdd);
        mAddCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(ActivityScan.this);
                alert.setTitle("Add code");
                final EditText input = new EditText(ActivityScan.this);
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                input.setRawInputType(Configuration.KEYBOARD_12KEY);
                alert.setView(input);
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (input.getText().length() > 0) {
                            try {
                                int code = Integer.parseInt(input.getText().toString());
                                mBarcodes.add(new BarCode(BarCode.CodeType.CODE_128, code));
                            } catch (NumberFormatException ex) {
                                Log.d("AddCode", "Not a number!");
                            }
                        }
                    }
                });
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //Put actions for CANCEL button here, or leave in blank
                    }
                });
                AlertDialog alertToShow = alert.create();
                alertToShow.getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                alertToShow.show();
            }
        });

        mClearBtn = (ImageButton) findViewById(R.id.imgBtnClear);
        mClearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mBarcodes.isEmpty()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(ActivityScan.this);
                    alert.setTitle("Are you sure you want to delete ALL codes?");
                    alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            mBarcodes.clear();
                            mTinydb.putListBarcodes("barcodes", mBarcodes);
                        }
                    });
                    alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            //Put actions for CANCEL button here, or leave in blank
                        }
                    });
                    alert.show();
                }
            }
        });

        mDeleteLastBtn = (ImageButton) findViewById(R.id.imgBtnDelete);
        mDeleteLastBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mBarcodes.isEmpty()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(ActivityScan.this);
                    alert.setTitle("Are you sure you want to delete last code: " + mBarcodes.get(mBarcodes.size() - 1) + "?");
                    alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (!mBarcodes.isEmpty()) {
                                mBarcodes.remove(mBarcodes.size() - 1);
                            }
                            //mTinydb.putListBarcodes("barcodes", mBarcodes);
                        }
                    });
                    alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            //Put actions for CANCEL button here, or leave in blank
                        }
                    });
                    alert.show();
                }
            }
        });

        mValidateBtn = (ImageButton) findViewById(R.id.imgBtnValidate);
        mValidateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String err = "No error!";
                int errCode = 0;
                if (!mBarcodes.isEmpty()) {
                    int max = 0;
                    ArrayList<Integer> positions = new ArrayList<Integer>();
                    int s = mBarcodes.size();
                    for(int i = 0; i < s; i++) {
                        BarCode bc = mBarcodes.get(i);
                        if ((i & 1) == 1) {
                            //even, should be a position code
                            if (bc.getCode() > 665) {
                                err = "Did not found a position code at line " + String.valueOf(i + 1) + "! Found " + String.valueOf(bc.getCode()) + " instead.";
                                errCode = 1;
                                break;
                            }
                            if (positions.contains(Integer.valueOf(bc.getCode()))) {
                                //we have duplicate positions
                                err = "Duplicate position code at line " + String.valueOf(i + 1) + "!";
                                break;
                            } else {
                                positions.add(Integer.valueOf(bc.getCode()));
                                if (bc.getCode() > max) max = bc.getCode();
                            }
                        } else {
                            //odd, should be a user code
                            if (bc.getCode() < 666) {
                                err = "Did not found a user code at line " + String.valueOf(i + 1) + "! Found " + String.valueOf(bc.getCode()) + " instead.";
                                errCode = 2;
                                break;
                            }
                        }
                    }
                    if ((errCode == 0) && (positions.size() > 0)) {
                        err = err + " Found " + String.valueOf(positions.size()) + " positions!";
                        //found no errors and at least an user code
                        ArrayList<Integer> missing = new ArrayList<Integer>();
                        for(int i = 1; i <= max; i++) {
                            if (!positions.contains(Integer.valueOf(i))){
                                missing.add(Integer.valueOf(i));
                            }
                        }
                        if (missing.size() > 0) {
                            err = err + " Missing positions: " + String.valueOf(missing.get(0));
                            if (missing.size() > 1) {
                                for (int i = 1; i < missing.size(); i++)
                                    err = err + ", " + String.valueOf(missing.get(i));
                            }
                            err = err + "!";
                        } else {
                            err = err + " NO missing positions!";
                        }
                    }
                    AlertDialog.Builder alert = new AlertDialog.Builder(ActivityScan.this);
                    alert.setTitle(err);
                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Log.d("ActivityScan", "");
                        }
                    });
                    alert.show();
                }
            }
        });

        //mRecognizerView.pauseScanning();
        barCodeText = (TextView) findViewById(R.id.textCode);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // you need to pass all activity's lifecycle methods to RecognizerView
        mRecognizerView.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // you need to pass all activity's lifecycle methods to RecognizerView
        mRecognizerView.resume();
        //mRecognizerView.pauseScanning();
        mTinydb = new TinyDB(this);
        mEvent = mTinydb.getString("eventName");
        mBarcodes = mTinydb.getListBarCodes("barcodes");
        Log.d(LOG_TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        // you need to pass all activity's lifecycle methods to RecognizerView
        mRecognizerView.pause();
        mTinydb.putListBarcodes("barcodes", mBarcodes);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // you need to pass all activity's lifecycle methods to RecognizerView
        mRecognizerView.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // you need to pass all activity's lifecycle methods to RecognizerView
        mRecognizerView.destroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // you need to pass all activity's lifecycle methods to RecognizerView
        mRecognizerView.changeConfiguration(newConfig);
    }

    @Override
    public void onScanningDone(RecognitionResults results) {
        // this method is from ScanResultListener and will be called when scanning completes
        // RecognitionResults may contain multiple results in array returned
        // by method getRecognitionResults().
        // This depends on settings in RecognitionSettings object that was
        // given to RecognizerView.
        // For more information, see chapter "Recognition settings and results")

        // After this method ends, scanning will be resumed and recognition
        // state will be retained. If you want to prevent that, then
        // you should call:
        // mRecognizerView.resetRecognitionState();

        // If you want to pause scanning to prevent receiving recognition
        // results, you should call:
        // mRecognizerView.pauseScanning();
        // After scanning is paused, you will have to resume it with:
        // mRecognizerView.resumeScanning(true);
        // boolean in resumeScanning method indicates whether recognition
        // state should be automatically reset when resuming scanning

        Log.d(LOG_TAG, "onScanningDone");

        BaseRecognitionResult[] dataArray = results.getRecognitionResults();
        if (dataArray != null) {
            for(BaseRecognitionResult baseResult : dataArray) {
                Log.d(LOG_TAG, "in for");
                if(baseResult instanceof BarDecoderScanResult) {
                    BarDecoderScanResult result = (BarDecoderScanResult) baseResult;

                    // getBarcodeType getter will return a BarcodeType enum that will define
                    // the type of the barcode scanned
                    BarcodeType barType = result.getBarcodeType();
                    // getStringData getter will return the string version of barcode contents
                    String barcodeData = result.getStringData();
                    // getRawData getter will return the raw data information object of barcode contents
                    BarcodeDetailedData rawData = result.getRawData();
                    // BarcodeDetailedData contains information about barcode's binary layout, if you
                    // are only interested in raw bytes, you can obtain them with getAllData getter
                    if (rawData != null) {
                        //byte[] rawDataBuffer = rawData.getAllData();
                        if (!lastCode.equals(barcodeData)) {
                            Log.d(LOG_TAG, "Scanned code: " + barcodeData);
                            ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                            toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
                            lastCode = barcodeData;
                            barCodeText.setText(barcodeData);
                            mScanAgainBtn.setEnabled(true);
                            mRecognizerView.pauseScanning();

                            //mTinydb.putInt("clickCount", 2);
                            mBarcodes.add(new BarCode(BarCode.CodeType.CODE_128, Integer.parseInt(lastCode)));
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onCameraPreviewStarted() {
        // this method is from CameraEventsListener and will be called when camera preview starts
    }

    @Override
    public void onCameraPreviewStopped() {
        // this method is from CameraEventsListener and will be called when camera preview stops
    }

    @Override
    public void onError(Throwable exc) {
        /**
         * This method is from CameraEventsListener and will be called when
         * opening of camera resulted in exception or recognition process
         * encountered an error. The error details will be given in exc
         * parameter.
         */
    }

    @Override
    @TargetApi(23)
    public void onCameraPermissionDenied() {
        /**
         * Called on Android 6.0 and newer if camera permission is not given
         * by user. You should request permission from user to access camera.
         */
        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_CAMERA_REQUEST_CODE);
        /**
         * Please note that user might have not given permission to use
         * camera. In that case, you have to explain to user that without
         * camera permissions scanning will not work.
         * For more information about requesting permissions at runtime, check
         * this article:
         * https://developer.android.com/training/permissions/requesting.html
         */
    }

    @Override
    public void onAutofocusFailed() {
        /**
         * This method is from CameraEventsListener will be called when camera focusing has failed.
         * Camera manager usually tries different focusing strategies and this method is called when all
         * those strategies fail to indicate that either object on which camera is being focused is too
         * close or ambient light conditions are poor.
         */
    }

    @Override
    public void onAutofocusStarted(Rect[] areas) {
        /**
         * This method is from CameraEventsListener and will be called when camera focusing has started.
         * You can utilize this method to draw focusing animation on UI.
         * Areas parameter is array of rectangles where focus is being measured.
         * It can be null on devices that do not support fine-grained camera control.
         */
    }

    @Override
    public void onAutofocusStopped(Rect[] areas) {
        /**
         * This method is from CameraEventsListener and will be called when camera focusing has stopped.
         * You can utilize this method to remove focusing animation on UI.
         * Areas parameter is array of rectangles where focus is being measured.
         * It can be null on devices that do not support fine-grained camera control.
         */
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        Log.d(LOG_TAG, "onDialogPositiveClick");
        mBarcodes = mTinydb.getListBarCodes("barcodes");
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        Log.d(LOG_TAG, "onDialogNegativeClick");
    }
}
