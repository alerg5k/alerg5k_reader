package com.alerg5k.reader;

import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.alerg5k.reader.util.TinyDB;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NewEventDialogFragment.NoticeDialogListener {

    private TinyDB mTinyDb;
    private String mEvent = "";
    private ArrayList<BarCode> mBarcodes;
    private boolean mHasEvent;
    private static final int MY_REQUEST_CODE = 1;
    private static final String LOG_TAG = "Alerg5K_Reader";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button scanCodeBtn = (Button) findViewById(R.id.newEventBtn);
        scanCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new NewEventDialogFragment().show(getFragmentManager(), "dialog");
            }
        });
        Button exportBtn = (Button) findViewById(R.id.exportBtn);
        exportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mHasEvent) {
                    sendEmail();
                }
            }
        });
        Button contEventBtn = (Button) findViewById(R.id.continueBtn);
        contEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mHasEvent) {
                    Intent intent = new Intent(MainActivity.this, ActivityScan.class);
                    // Starting Activity
                    startActivityForResult(intent, MY_REQUEST_CODE);
                }
            }
        });
        TextView tvVersion = (TextView) findViewById(R.id.tvVersion);
        tvVersion.setText(BuildConfig.VERSION_NAME);
        mTinyDb = new TinyDB(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHasEvent = mTinyDb.getBoolean("haveEvent");
        TextView tvEvent = (TextView) findViewById(R.id.txtEvent);
        Button btnExport = (Button) findViewById(R.id.exportBtn);
        if (mHasEvent) {
            Log.d(LOG_TAG, "loading event data");
            mEvent = mTinyDb.getString("eventName");
            Log.d(LOG_TAG, "event name: " + mEvent);
            mBarcodes = mTinyDb.getListBarCodes("barcodes");
            for (BarCode b : mBarcodes) {
                Log.d(LOG_TAG, "loaded barcode: " + String.valueOf(b.getCode()));
            }
            tvEvent.setText("Current event: " + mEvent);
            btnExport.setEnabled(true);
        } else {
            Log.d(LOG_TAG, "no event");
            mEvent = "";
            mBarcodes = new ArrayList<BarCode>();
            tvEvent.setText("No event");
            btnExport.setEnabled(false);
        }

    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        if (dialog instanceof NewEventDialogFragment) {
            //starting new event
            // Intent for Pdf417ScanActivity Activity
            Intent intent = new Intent(MainActivity.this, ActivityScan.class);

            // Starting Activity
            startActivityForResult(intent, MY_REQUEST_CODE);
            mTinyDb.putBoolean("haveEvent", true);
            mTinyDb.putString("eventName", ((NewEventDialogFragment) dialog).getEventName());
            mTinyDb.putListBarcodes("barcodes", new ArrayList<BarCode>());
            //mHasEvent = true;
        } else {
            Log.d(LOG_TAG, "Positive click from unknown dialog");
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        if (dialog instanceof NewEventDialogFragment) {
            Log.d(LOG_TAG, "negative click from new event dialog");
        } else {
            Log.d(LOG_TAG, "negative click from unknown dialog");
        }
    }

    private void sendEmail() {
        File file   = null;
        File root   = this.getFilesDir();//getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        if (root.canWrite()){

            file   =   new File(root, "Barcodes-" + mEvent + ".csv");
            FileOutputStream out   =   null;
            try {
                out = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            for (BarCode bCode:mBarcodes) {
                try {
                    out.write(bCode.toExportString().getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (file != null) {
            Uri u1 = null;
            u1 = FileProvider.getUriForFile(MainActivity.this, BuildConfig.APPLICATION_ID + ".provider", file);

            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Barcodes " + mEvent);
            sendIntent.putExtra(Intent.EXTRA_STREAM, u1);
            sendIntent.setType("text/html");

            startActivity(sendIntent);
        } else {
            Log.d(LOG_TAG, "file is null, could not export. Check write permissions on " + Environment.getExternalStorageDirectory());
        }
    }
}